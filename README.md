# Lily's Manual

An instruction manual for our dog Lily

## Intro

Lily is a very cuddly dog and just loves to be around us all the time. Thank you for watching her while we' re away!

## General daily routine

* Around 8am-9am: anywhere between 30-60min walk in the park + play with ball
* Upon arriving home, she likes to be rubbed with a towel (including her paws) and then breakfast (see Food below) - please take the ball off her and keep it from her otherwise she will destroy it!
* After that, she usually naps or follows us around.
* Around 12pm-1pm: walk/loo break (10-20min usually)
* 4:30pm Dinner (see Food below)
* Around 6pm-7pm: walk/loo break (10-20min usually)
* Another quick pee before bed

## Food

She can look like she's always hungry and asking for your food, but don't fall into her drama! She seems to know when it's dinner time but please wait until at least 4:30pm. 
100g of kibble (= 1 level measure of the blue cup, for each meal).
Prepare the food by pouring kibble on the blue bumpy feeder and pour a bit of cold water (a shot glass is enough) to soften it a bit.
Replace the water bowl with fresh cold water.
Ask her to sit ("SIT!") and place both bowls on the floor and say "WAIT!". She will be looking at you waiting for the order to go for it: then you can firmly say "OK!" and she will go for it. 

### Treats

Absolutely NO:
* chocolate (highly toxic for dogs)
* chicken bones (they produce shards and are dangerous)
* salty food

OK but optional:
* raw broccoli (small bit)
* carrot (1/2 carrot)
* tinned sardines or tuna (1/2 a tin)
* dog treats (dental sticks, sprats, camomile biscuits)

She can be grabby so careful when hand-feeding her. It's safer to put somewhere and tell her to wait "WAIT!" an then "OK!" like above. 

More recently she's been trying to steal food from the counter or coffee tables when left alone. Careful leaving food on those places, especially forbidden items (e.g. chocolate).

## Sleep

She sleeps in her bed in the kitchen with the door closed, but the bed can be wherever works for you, as long as it's not next to any draft or radiator. Usually she sleeps the whole day on a sofa or her bed. But when it's time for everyone to go to bed, we say "Lily, let's go to bed?" and she will follow us there. She normally stands next to the bed for you to lift her blanket so she can jump underneath and completely wrap herself in it. Yes, she does like to be totally covered!


## Outdoors and socialising 

* People: OK with people, including kids
* Other dogs: NOT OK especially with bigger dogs or small but aggitated ones. She gets anxious and might growl, snap or even bite. Avoid completely. She prefers her own space and mostly ignores them
* She can be picky with where she pees/poos, normally has to be a grassy or leafy area

### Off the lead

She pulls quite a bit but we've been trying to educate her by stopping and saying "back", and she will reverse a bit.
Lily has good enough recall but we'd prefer you to keep her on the lead until she gets to know you better and it's safe.

She's obsessed with squirrels and can lunge suddenly when she sees one, so just be aware.


## Behaviour, health and safety, emergencies

If she behaves badly, absolutely no violence, no slapping, yanking the lead or shouting. Sometimes she just eats stuff off the floor and we can just pull her away from it and say "UH-UH" and "drop it".

In case there's any incident with another dog, please take the owner's contact details and contact us immediately.

In case there's any accident or she becomes unwell in a way that needs emergency medical attention, please contact us immediately. Her vet practice is the [Roundhouse Veterinary Hospital](https://www.petsnvets.org/our-practices/the-roundhouse-veterinary-hospital): 43-47 Cogan Road, Auldhouse Retail Park, Pollokshaws, G43 1BJ, UK | Phone: 0141 649 4949


